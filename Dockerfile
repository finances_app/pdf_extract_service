FROM alpine:3.8
EXPOSE 80

RUN apk update && apk upgrade

# install node and yarn
RUN apk add nodejs-current
RUN apk add yarn

# pdf-extract dependencies
RUN apk add pdftk
RUN apk add poppler-utils
RUN apk add ghostscript
RUN apk add tesseract-ocr

# Timezone tool
RUN apk add tzdata
ENV TZ=America/Toronto
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN mkdir /lib/pdf_extract
WORKDIR /lib/pdf_extract

# explicitely copy the config first to trigger a yarn install only when needed
COPY package.json package.json
RUN yarn

COPY . .

CMD ["node", "server.js"]