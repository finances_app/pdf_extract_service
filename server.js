const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');
const util = require('util');
const pdf_extract = require('pdf-extract');

const fs_writeFile = util.promisify(fs.writeFile);
const fs_unlink = util.promisify(fs.unlink);

// Setup a simple http listener for inter-container queries
const app = express();

app.use(bodyParser.urlencoded({ extended: true, limit: '5mb' }));
app.use(bodyParser.json({ limit: '5mb' }));

app.post('/extract', async (req, res) => {
    const encodedFile = req.body.file;

    const filePath = path.join(__dirname, 'input.pdf');

    try {
        let buffer = Buffer.from(encodedFile, 'base64');
        await fs_writeFile(filePath, buffer);

        let extractedData = await new Promise((resolve, reject) => {
            let processor = pdf_extract(filePath, { type: 'text' }, function (err) {
                if (err) reject(err);
            });
            processor.on('complete', async function (data) {
                resolve(data);
            });
            processor.on('error', function (err) {
                reject(err);
            });
        });

        res.send(extractedData);
        console.log(`[${new Date().toLocaleString('en-CA')}] file processed (hash:${extractedData.hash})`);
    } catch (err) {
        console.error(err);
        res.status(500).send({
            message: err.message,
            stack: err.stack,
        });
    }

    // cleanup input file
    try {
        await fs_unlink(filePath);
    } catch (_) {}
});

app.listen(process.env.PORT || 80);
